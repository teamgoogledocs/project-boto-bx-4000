const Discord = require('discord.js');
const {prefix} = require('../../config.json');
const request = require("request");

module.exports = {
  name: 'trad',
  alias: ['traduction', 'translate'],
  description: 'Traduit un mot ou une phrase',
	serverOnly: false,
	cooldown: 1,
  args: true,
	usage: '[langue] [mots]',
  execute(message, args){
    if (args.length < 2) return message.channel.send("`❌ Wrong parameters. Usage: " + prefix + "trad [langue] [text]`")
    var l1 = args[0];
    //var l2 = args[1];
    //console.log(l1 + '-' + l2);
    var text = '';
    for (var i=1; i<args.length; i++) text += args[i] + '%20';
    var keys = require('../../utils/apiKeys.json');
    let urlTranslate = 'https://translate.yandex.net/api/v1.5/tr.json/translate' +
                       '?key=' + keys.TranslateKey +
                       '&text=' + text +
                       '&lang=' + l1; // + '-' + l2;

    console.log(urlTranslate);
    request({
      url: urlTranslate,
      json: true
    }, function (error, response, body) {
      console.log(body);
      if (!error && response.statusCode === 200){
        return message.channel.send('` Résultat: ` ' + body.text[0]);
      }
      else {
        return message.channel.send('`❌ Error: ' + body.message + '`');
      }
    });
  }
}
