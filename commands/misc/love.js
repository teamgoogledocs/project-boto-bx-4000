const Discord = require('Discord.js');
const Jimp = require('Jimp');
const fs = require("fs");

function createImage(message, luck, ajd, user){

	// Création de l'ojet image avec comme couleur de fond #131313
	var width = 1032;
	var height = 256;
	const imageLuck = new Jimp(width, height, '#131313', function(err, image){
		if (err) throw err;
		// ((999-285)/1032)*512 = 354
		// Création des bordures de la barre de progression
		for (var y=85; y<147; y++){
			image.setPixelColor(0xFFFFFFFF, 285, y);
			image.setPixelColor(0xFFFFFFFF, 1000, y);
		}
		for (var x=285; x<1000; x++){
			image.setPixelColor(0xFFFFFFFF, x, 85);
			image.setPixelColor(0xFFFFFFFF, x, 147);
		}

		// Remplissage en fonction de la luck
		var widthRec = (luck / 100) * 715;
		for (var xRec=286; xRec<286 + widthRec; xRec++){
			for (var yRec=86; yRec<146; yRec++){
				image.setPixelColor(0xFF5500FF, xRec, yRec);
			}
		}
	});

	// Check si l'avatar est un gif ou non
	var typeAvatar = (user.avatarURL.split('.')[3] == "gif") ? "gif" : "png";

	// Récupération de l'avatar du user
	var avatarUrl;
	if (typeAvatar == "png") avatarUrl = user.avatarURL.split('=')[0] + '=256';
	else avatarUrl = __dirname + '../../../datas/miscs/ckc.png';

	// Le font doit être chargé avant la promise
	const fontLoadPromise = Jimp.loadFont(Jimp.FONT_SANS_32_WHITE);
	return Promise
	        .all([
							// 1) Transformation de l'avatar en objet image reconnu par Jimp
							// 2) Image de base avec la pgbar créée au-dessus
							// 3) Font du texte à coller
							// 4) Cadre qui entoure l'image
	            Jimp.read(avatarUrl),
	            imageLuck,
	            fontLoadPromise,
							Jimp.read(__dirname + '../../../datas/miscs/cadreLuck.png')
	        ])
	        .then(function (results) {
	            const avatar = results[0];
	            const baseImage = results[1];
	            const font = results[2];
							const cadreLuck = results[3];

							// Resize de l'avatar pour faire du 256 par 256
							var resizedAvatar = avatar.scaleToFit(256, 256);

							// Collage du texte et de l'avatar sur l'image de base
	            baseImage
	                .print(font, 300, 30, "Chance pour " + user.username + " :")
									.print(font, 600, 100, luck.toString() + " %")
									.print(font, 430, 190, "Prédiction pour " + ajd)
									.blit(resizedAvatar, 0, 0);
							return cadreLuck.blit(baseImage, 256, 25);
	        })
					// Envoi du buffer de l'image
	        .then(function(compositedImage){
						compositedImage.getBuffer(Jimp.MIME_PNG, (err, buffer) => {
							if (err) throw err;
							message.channel.send({files: [buffer] });
						});
					})
					// Gestion d'erreur
	        .catch(err => {
	            console.log(err);
	            console.log(err.stack);
	            throw err;
	        });
}

module.exports = {
	name: 'love',
	alias: ['luv', 'amour'],
	description: 'Calcule le taux de compatibilité entre deux utilisateurs',
	serverOnly: true,
	cooldown: 1,
	usage: '[@user1] [@user2]',
	execute(message, args) {

		if (!(message.mentions.users.size == args.length && args.length == 2)) return message.channel.send('nope');
		const id1 = message.mentions.users.first().id;
		const id2 = message.mentions.users.last().id;
		console.log(id1);
		console.log(id2);
		const id12 = (parseInt(id1) + parseInt(id2)).toString();
		console.log(id12);
		let sum = 0;
		for(let i = 0; i < id12.length; i++) {
			if([0, 1, id12.length - 2, id12.length - 1].includes(i)) {
				sum -= parseInt(id12.charAt(i));
			}
			else {
				sum += parseInt(id12.charAt(i));
			}
			console.log(`${i} : ${sum}`);
		}

		const luck = 50;

		// Puis on crée l'image et on l'envoie dans la fonction
    //createImage(message, luck, 'ojd', user);
	},
};
