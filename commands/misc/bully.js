const Discord = require('Discord.js');

const rights = require('../../utils/rightcheck.js')
module.exports = {
	name: 'bully',
	description: 'Bully ***TOUT LE MONDE***',
	serverOnly: true,
  usage: "[nb secondes] ([user] : optionel)",
	cooldown: 1,
  args: true,
	execute(message, args) {

    if (rights.isOwner(message.author)){
      var sec = args[0] * 1000;
      var interval;
      var argUser = false;
      setTimeout(function(){clearInterval(interval);}, sec);

      var interval = setInterval (function () {
          var users = message.guild.members.random(message.guild.memberCount);
          var user = users[0];
          while (user.user.bot) {
            users = message.guild.members.random(message.guild.memberCount);
            user = users[0];
          }
          if (args.length === 2) { user = args[1]; argUser = true; }

          var channel = message.guild.channels.random(message.guild.channels.length);
          while (channel.type != "text" ) channel = message.guild.channels.random(message.guild.channels.length);
          if (!argUser) message.guild.channels.get(channel.id).send("<@!" + user.id + ">").then(msg => { msg.delete(); });
          else message.guild.channels.get(channel.id).send(user).then(msg => { msg.delete(); });
          setTimeout(function(){}, 1000);
  		}, 1 * 1000);
    }

	},
};
