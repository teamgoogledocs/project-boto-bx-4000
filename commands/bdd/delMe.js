const Discord = require('Discord.js');
const {user} = require ('../../utils/routes');
const rights = require('../../utils/rightcheck.js')
const request = require("request");

module.exports = {
	name: 'delme',
	description: 'Kill me please',
	execute(message, args) {
            request.delete({
            url: user.delete(message.author.id),
            json: true
        }, function (error, response, body) {

            if (response.statusCode === 201) {
                message.channel.send(`Ciao ${body.pseudo}!`)
            } else if (response.statusCode === 404) {
                message.channel.send("Vous n'êtes déjà rien a mes yeux, je ne peux pas plus vous supprimer...")
            } else {
                message.channel.send("Sorry something went wrong...")
            }
        })
    },
};