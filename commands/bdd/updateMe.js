const Discord = require('Discord.js');
const {user} = require ('../../utils/routes');
const rights = require('../../utils/rightcheck.js')
const request = require("request");

module.exports = {
	name: 'updateme',
	description: 'Me mets à jour dans la bdd',
	execute(message, args) {
        if (args[0]){
            var userToAdd = { 'pseudo':args[0] } 
                request.put({
                url: user.put(message.author.id),
                body: userToAdd,
                json: true
            }, function (error, response, body) {
    
                // If it responded correctly
                if (response.statusCode === 201) {
                     return message.channel.send(`Bravo ${body.pseudo}! You have been updated in the database!`)
                } else if (response.statusCode === 404) { 
                    message.channel.send("Avant de vouloir te mettre à jour existe un peu pour voir...")
                } else {
                    message.channel.send("Sorry something went wrong...")
                }
            })

        } else {
            message.channel.send("Veuillez renseigner un nouveau pseudo...")
        }
    }
};