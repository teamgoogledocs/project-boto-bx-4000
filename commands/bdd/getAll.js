const Discord = require('Discord.js');
const {user} = require ('../../utils/routes');
const rights = require('../../utils/rightcheck.js')
const request = require("request");

module.exports = {
	name: 'getall',
	description: 'Mais qui est dans la bdd?',
	execute(message, args) {
            request.get({
            url: user.getAll(),
            json: true
        }, function (error, response, body) {

            var users = "Voici les individus présents dans la base de données :\n"
            body.forEach(user => {
                users += user.pseudo + '\n'
            });
            message.channel.send(users)
        })
    },
};