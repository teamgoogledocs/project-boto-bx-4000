const Discord = require('Discord.js');
const {user} = require ('../../utils/routes');
const rights = require('../../utils/rightcheck.js')
const request = require("request");

module.exports = {
	name: 'addme',
	description: 'Me rajoute dans la bdd',
	execute(message, args) {

        var userToAdd = { 'pseudo':message.author.username, 'discord_id':message.author.id } 
            request.post({
            url: user.post(),
            headers:{'Content-Type': 'application/json'}, 
            body: userToAdd,
            json: true
        }, function (error, response, body) {

            if (response.statusCode === 201) {
                if (body.estDansBdd) { return message.channel.send(`Hey! Tu es déjà dans la base de données!`)}
                else {return message.channel.send(`Bravo ${body.pseudo}! You are now living in the database!`)}
            }
            else return message.channel.send(`Something went wrong when fetching for data`)
        })
    },
};