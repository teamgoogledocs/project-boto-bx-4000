const Discord = require('Discord.js');
const {user} = require ('../../utils/routes');
const rights = require('../../utils/rightcheck.js')
const request = require("request");

module.exports = {
	name: 'getme',
	description: 'Mais qui est dans la bdd?',
	execute(message, args) {
            request.get({
            url: user.get(message.author.id),
            json: true
        }, function (error, response, body) {
            console.log(response.statusCode)
            if (response.statusCode === 201) {
                message.channel.send(`AHAHAH Je te tiens ${body.pseudo}! Ton id discord est ${body.discord_id}`)
            } else if (response.statusCode === 404) {
                message.channel.send("Vous n'éxistez pas dans la bdd veuillez faire un b!addme")
            } else {
                message.channel.send("Sorry something went wrong...")
            }
        })
    },
};