const Discord = require('discord.js');
const User = require('../../datas/classesXp/user.js');
const rights = require('../../utils/rightcheck.js')

module.exports = {
	name: 'getlvl',
	description: 'Donne le lvl de l\'user',
	serverOnly: true,
	cooldown: 0.1,
	execute(message, args) {
		/*var text = "```md\n"
		for (var i=1; i<51; i++) {
			text += "* level " + i + ": < " + (25 * i * i) + " >\n";
		}
		return message.channel.send(text + "```");*/

    var member = message.author;
		message.mentions.users.forEach(function(guy){ member = guy });
    var user = User.getUser(member.id);
    if (user == null) {
      user = new User(member.id, message.guild.id, 0, 0);
    }
    return message.channel.send('`Level de ' + member.username + ': ' + user.getXpLevel() + ', xp: ' + user.xp + '\nLevel de voleur: ' + user.getThiefLevel() + ', xp: ' + user.thiefXp + '`');
	},
};
