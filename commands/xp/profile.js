const Discord = require('discord.js');
const Jimp = require("jimp");
const User = require("../../datas/classesXp/user.js");
const Server = require("../../datas/classesXp/server.js");
const fs = require('fs');

module.exports = {
  name: "profile",
  description: "Affiche le profile de l'utilisateur",
  serverOnly: true,
  usage: '([user] optionel)',
  execute(message, args){
    // On récupère la personne dont on veut afficher le profile
    var member = message.author;
    if (args.length > 0) member = message.mentions.users.first();

    // On récupère le user associé
    var user = User.getUser(member.id);
    if (user == null) user = User.baseUser(member.id, message.guild.id);

    // ainsi que le server pour vérifier qu'il peut faire cette commande
    var serv = Server.getServer(message.guild.id);
    if (serv == null) serv = Server.baseServer(message.guild.id);
    if (!serv.acceptXp) return message.channel.send("`❌ Ce serveur n'accèpte pas l'utilisation de l'xp`");
    if (!serv.acceptThief) return message.channel.send("`❌ Version sans xp de voleur en cours de dev, soz`");

    // puis on lance la fonction qui va créer l'image et l'envoyer
    createImage(message, member, user, serv);
  },
}

// Vérifie si le point correspond au bord du cercle de rayon donné
function isBorderCircle(center, radius, x, y){
  var dist = Math.floor(Math.sqrt(((center.x - x) ** 2) + ((center.y - y) **2)));
  return dist == radius -1 || dist == radius || dist == radius + 1;
}

// Vérifie si le point est entre les 2 cercles donnés
function isInDisk(center, radiusD1, radiusD2, x, y){
  var distPoint = Math.floor(Math.sqrt(((center.x - x) ** 2) + ((center.y - y) **2)));
  return distPoint < radiusD1 && distPoint > radiusD2;
}

// Vérifie si le point est dans la zone à colorier
function isInPercent(coords, x, y){
  return (x <= coords.droite.xMax && x >= coords.droite.xMin
          && y <= coords.droite.yMax && y >= coords.droite.yMin) ||
          (x <= coords.gauche.xMax && x >= coords.gauche.xMin
          && y <= coords.gauche.yMax && y >= coords.gauche.yMin);
}

// Vérifie si le point ne dépasse pas la ligne de fin de zone à colorier
function isBehindLine(coordsDisques, x, y, percent){
  var xD1 = coordsDisques.xCercle1;
  var yD1 = coordsDisques.yCercle1;
  // droite : y = ax + b, donc on trouve a :
  var a = ((296 - yD1) / (296 - xD1));

  // En suite on regarde en fonction du %, de x et de y s'ils sont avant ou après la ligne
  var yLim = a*(x-xD1) + yD1;
  var xLim = xD1 + ((y - yD1)/a);
  if (percent < 25) {
    if (x>296 && y<297) return y < yLim && x < xLim;
    else return false;
  }
  else if (percent < 50) {
    if (x>296 && y>296) return y < yLim && x > xLim;
    else if (x>296 && y<297) return true;
    else return false;
  }
  else if (percent < 75){
    if (x < 296 && y > 296) return y > yLim && x > xLim;
    else if (x>295) return true;
    else return false;
  }
  else {
    if (x < 296 && y < 297) return y > yLim && x < xLim;
    else if ((x<296 && y>296) || x>295) return true;
    else return false;
  }
}

// Crée l'image et l'envoie
function createImage(message, target, user, serv){
  // Taille de l'image et constante de conversion des degrés vers les radians
  const size = 592;
  const CELSIUS_TO_RADIAN = 0.0174532925 // = 1° en radian

  // Calcul du pourcentage d'xp de l'user et conversion en un angle en radians
  const xpPercent = Math.floor(((user.xp  - Server.previousLvl(user.xp))/ (Server.nextLvl(user.xp) - Server.previousLvl(user.xp)) * 100));
  const volPercent = Math.floor(((user.thiefXp  - Server.previousLvl(user.thiefXp))/ (Server.nextLvl(user.thiefXp) - Server.previousLvl(user.thiefXp)) * 100));
  var a1 = ((360 / 100) * (xpPercent-25)) * CELSIUS_TO_RADIAN;
  var a2 = ((360 / 100) * (volPercent-25)) * CELSIUS_TO_RADIAN;

  // Calcul des coordonnées des points des disques qui correspondent à ce pourcentage
  var coordsDisqueXp = {xCercle1: (size/2) + (296 * Math.cos(a1)), yCercle1: (size/2) + (296 * Math.sin(a1)),
                        xCercle2: (size/2) + (264 * Math.cos(a1)), yCercle2: (size/2) + (264 * Math.sin(a1))};
  var coordsDisqueVol = {xCercle1: (size/2) + (238 * Math.cos(a2)), yCercle1: (size/2) + (238 * Math.sin(a2)),
                         xCercle2: (size/2) + (212 * Math.cos(a2)), yCercle2: (size/2) + (212 * Math.sin(a2))};

  // Création de l'image
  const img = new Jimp(size, size, 0x00000000, function(err, image){
		if (err) throw err;

    var center = {x: size/2, y: size/2};
    for (var x=0; x<size; x++) {
      for (var y=0; y<size; y++) {
        // Contours des cercles pour former 2 disques
        if (isBorderCircle(center, 296, x, y) || isBorderCircle(center, 264, x, y)
            || isBorderCircle(center, 238, x, y) || isBorderCircle(center, 212, x, y))
          image.setPixelColor(0x00000000, x, y);
        else if (Math.floor(Math.sqrt(((center.x - x) ** 2) + ((center.y - y) **2))) < 296)
          image.setPixelColor(0x131313FF, x, y);

        // Remplissage des disques
        if (isInDisk(center, 296, 264, x, y)
            && isBehindLine(coordsDisqueXp, x, y, xpPercent))
          image.setPixelColor(0xFF5500FF, x, y);
        else if (isInDisk(center, 296, 264, x, y))
          image.setPixelColor(0x200D00FF, x, y)
        if (isInDisk(center, 238, 212, x, y)
            && isBehindLine(coordsDisqueVol, x, y, volPercent))
          image.setPixelColor(0x00A2E8FF, x, y);
        else if (isInDisk(center, 238, 212, x, y))
          image.setPixelColor(0x001722FF, x, y)
      }
    }
	});

	// Le font doit être chargé avant la promise
	const fontLoadPromise = Jimp.loadFont(Jimp.FONT_SANS_32_WHITE);
  return Promise
          // on charge une image de fond, l'image qu'on vient de créer, les flèches et le font
	        .all([
	            Jimp.read(__dirname + '../../../datas/classesXp/baseProfile.png'),
	            img,
	            fontLoadPromise,
              Jimp.read(__dirname + '../../../datas/classesXp/flecheOrange.png'),
              Jimp.read(__dirname + '../../../datas/classesXp/flecheBleue.png')
	        ])
          // Puis on print tout le texte et on colle l'image de fond sur l'image créée
	        .then(function (results) {
	            const baseProfile = results[0];
	            const baseImage = results[1];
	            const font = results[2];

              // On tourne les flèches d'autant de degrés qu'il faut dans le sens anti-horaire
              var xpDeg = ((360 / 100) * (xpPercent)) * (-1)+0.3;
              var volDeg = ((360 / 100) * (volPercent)) * (-1)+0.3;

							// Collage du texte et de baseProfile sur l'image de base
              var strVolLvl = user.getThiefLevel().toString();
	            return baseImage
	                .print(font, 340, 140, user.getXpLevel())
									.print(font, 120, 195, {text: target.username, alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER}, 356, 40)
									.print(font, 170, 245, user.xp + '/' + Server.nextLvl(user.xp))
                  .print(font, 170, 308, user.thiefXp + '/' + Server.nextLvl(user.thiefXp))
                  .print(font, 120, 450, {text: strVolLvl, alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER},356,40)
									.blit(baseProfile, 0, 0);
	        })
					// Envoi du buffer de l'image
	        .then(function(compositedImage){
						compositedImage.getBuffer(Jimp.MIME_PNG, (err, buffer) => {
							if (err) throw err;
							message.channel.send({files: [buffer] });
						});
					})
					// Gestion d'erreur
	        .catch(err => {
	            console.log(err);
	            console.log(err.stack);
	            throw err;
	        });
}
