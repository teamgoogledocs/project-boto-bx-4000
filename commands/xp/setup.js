const Discord = require('discord.js');
const rights = require('../../utils/rightcheck.js');
const {prefix} = require('../../config.json');
const modules = { accept: require('./setup/accept.js') };

module.exports = {
	name: 'setup',
	description: 'Setup du serveur pour l\'xp',
	serverOnly: true,
	execute(message, args) {
		if (rights.isAdmin(message.member)){
			if (args.length == 0) embedSetup(message);
			else {
				switch (args[0]) {
					case 'acceptXp': modules['accept'].execute(message, args.slice(1), "xp");
						break;
					case 'acceptVol': modules['accept'].execute(message, args.slice(1), "vol");
						break;
					default: embedSetup(message);
				}
			}
	  }
	  else return message.channel.send("`❌ Tu ne peux pas configurer ce bot sur ce serveur...``")
	},
};

function embedSetup(message){
	let embed = new Discord.RichEmbed()
	.setAuthor(message.client.user.username, message.client.user.avatarURL)
	.setColor("#FFAA00")
	.setTitle("Configuration de l'xp sur ce serveur")
	.setThumbnail(message.client.user.avatarURL)
	.addField("`" + prefix + "setup acceptXp [oui|non]`", "Accepte ou non l'xp sur le serveur.\n឵")
	.addField("`" + prefix + "setup acceptVol [oui|non]`", "Accepte ou non le vol d'xp sur le serveur.\n឵")
	.setFooter(message.client.user.username + ' by #TeamGDocs • Today at ' + new Date().toLocaleTimeString(), message.client.user.avatarURL);
	 return message.channel.send(embed);
}
