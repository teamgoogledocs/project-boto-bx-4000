const fs = require('fs');
const status = require('../../utils/status.json');

module.exports = {
	name: 'enable',
	description: 'Active une commande, si elle est déjà activé ne fait concrètement rien',
	execute(message, args) {
		if(!message.client.commands.has(args[0])) return message.reply('lmao tékon ou tu le fais exprès ? ta commande existe pas');

		//	on active la commande.
		message.client.commands.get(args[0]).status = true;
		status[args[0]] = true;

		//	comme owner est applé dans index qui se trouve dans la racine, on doit faire le chemin relatif à ce fichier là
		fs.writeFileSync('./utils/status.json', JSON.stringify(status, null, 2));
		message.channel.send('La commande a été activé.');
	},
};
