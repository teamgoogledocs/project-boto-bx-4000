module.exports = class SkullPlayer {
	constructor(user) {
		//	Je crois que les attributs sont assez explicite
		this.id = user.id;
		this.name = user.username;
		this.points = 0;
		this.cards = [1, 1, 1, 0];
		this.stack = [];
		this.canBet = true;
		this.revealedCards = [];
	}

	endTurn() {
		this.stack = [];
		this.revealedCards = [];
		this.canBet = true;
	}

	pass() {
		this.canBet = false;
	}

	isAlive() {
		return this.cards.length > 0;
	}

	getCardInHand() {
		const tmp = Array.from(this.cards);
		this.stack.forEach((e) => {
			if(tmp.includes(e)) tmp.splice(tmp.indexOf(e), 1);
		});

		let res = '';
		if (tmp.length == 0) res += '\0';
		tmp.forEach((e) => {
			if(e == 1) res += '💮';
			if(e == 0) res += '💀';
		});
		return res;
	}


	getName() {
		return this.name;
	}

	addPoint() {
		this.points++;
	}

	popStack() {
		this.revealedCards.push((this.stack.pop()));
	}

	getStackText() {
		let res = '';
		if (this.stack.length == 0) res += '\0';
		this.stack.forEach(() => {
			res += '⚛';
		});
		res += '\n';
		this.revealedCards.forEach((e) => {
			if(e == 1) res += '💮';
			if(e == 0) res += '💀';
		});
		return res;
	}

	isFlowerBet() {
		let res = true;
		//	Le nombre de fleur dans la pile du joueur
		let numberOfFlowerInStack = 0;

		//	On compte le nombre de fleurs dans la pile
		this.stack.forEach((elem) => {
			if (elem == 1) numberOfFlowerInStack++;
		});

		//	Le nombre de fleurs restant au joueur
		let numberOfFlower = this.cards.length;

		//	Si il y a un 0 dans les cartes du joueurs, on ajuste.
		if (this.cards.includes(0)) {
			numberOfFlower -= 1;
		}

		if (numberOfFlower == 0) res = false;
		if (numberOfFlower <= numberOfFlowerInStack) res = false;

		return res;
	}
 
	isSkullBet() {
		return this.cards.includes(0) && !(this.stack.includes(0));
	}

	stackCard(card) {

		//	Si le joueur veut mettre un crane
		if (card == 0) {
			if (this.stack.includes(0)) throw 'Skull already in stack';
			if (!this.cards.includes(0)) throw 'Player has no skull';

			this.stack.push(card);
		}
		else if (this.isFlowerBet()) {
			this.stack.push(card);
		}

	}

	removeCardMan(type) {
		this.cards.splice(this.cards.indexOf(type), 1);
	}

	removeCard(index) {
		//	On supprime une carte....
		this.cards.splice(this.cards.indexOf(this.shuffleCards()[index]), 1);
	}

	shuffleCards() {
		//	Fonction pour mélanger les cartes (Algorithme de Fisher-Yates)
		let j, x, i;
		const a = Array.from(this.cards);
		for (i = a.length - 1; i > 0; i--) {
			j = Math.floor(Math.random() * (i + 1));
			x = a[i];
			a[i] = a[j];
			a[j] = x;
		}
		return a;
	}
};
