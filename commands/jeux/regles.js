const fs = require('fs');
const games = fs.readdirSync('./datas/jeux');
const Discord = require('discord.js');

module.exports = {
	name: 'regles',
	alias: ['rules', 'regle', 'jr'],
	args: true,
	usage: '[jeu]',
	description: 'Envoie les règles du jeu passé en paramètre',
	execute(message, args) { 
		if (!games.includes(args[0])) return message.reply('lmaoo non.');
		const rules = require(`../../datas/jeux/${args[0]}/regles.json`);
		const rulesEmbed = new Discord.RichEmbed();
		rulesEmbed.setTitle(rules['Titre'])
			.setThumbnail(rules['Image'])
			.setAuthor('Le maître du jeu', rules['Thumbnail'])
			.setColor(rules['Couleur'])
			.setFooter(`${message.client.user.username}  by #TeamGDocs`, message.client.user.displayAvatarURL);

		Object.keys(rules).forEach(key => {
			if(!(key === 'Titre' || key === 'Image' || key === 'Couleur' || key === 'Thumbnail')) {
				rulesEmbed.addField(key, rules[key], true);
			}
		});
		message.author.send(rulesEmbed);

	},
};
