const Discord = require('discord.js');

function getImage(message, args) {
	// We use "request" module
	var request = require("request");
	var url = "https://nekos.life/";
	var lewd = false;
	if (args.length > 0 && (args[1] === "lewd" || args[0] === "lewd")) {
			if (!message.channel.nsfw)
					return message.reply("`❌ Seems like it's a christian channel here...`");
			url += "lewd";
			lewd = true;
	}

	// Then we request a json response from the site
	request({
			url: url,
			json: true
	}, function (error, response, body) {
				if (!error && response.statusCode === 200){
						let imageURL;
						if (!lewd){
								/* Some black magic explained :
								 * line 91 of the array = line of the image in html
								 * split space and take the fifth : there are some tabs and 5th is the src="url"
								 * split = and take the url part without the src= part
								 * then convert it into a string and substring to not have the ""
								*/
								let stringURL = body.split("\n")[91].split(" ")[5].split("=")[1];
								imageURL = stringURL.toString().substring(1, stringURL.length-1);
						}
						else {
								let stringURL = body.split("\n")[87].split(" ")[5].split("=")[1];
								imageURL = stringURL.toString().substring(1, stringURL.length-1);
						}
						let nekoEmbed = new Discord.RichEmbed()
						.setColor("#FFAA00")
						.setImage(imageURL);
						return message.channel.send(nekoEmbed);
				}
				else
						return message.channel.send("`❌ Something went wrong...`");
	});
}
module.exports = {
	name: 'neko',
	description: 'Send a beautiful neko image <3 (Can add `lewd` parameter to have a lewd neko image)',
	serverOnly: false,
	repertory: 'commands/pics',
	usage: "([lewd] : optional)",
	execute(message, args) {
			var minutes = 1;
			if (args.length == 2) minutes = parseInt(args[1]);
			var interval;
			setTimeout(function(){clearInterval(interval);}, minutes * 60 * 1000);
			getImage(message, args);
			var interval = setInterval (function() {
				getImage(message, args);
			}, 60 * 1000);
	},
};
