const fs = require('fs');
const Discord = require('discord.js');

module.exports = {
	name: 'jet',
	alias: ['jet', 'roll', 'jets'],
    args: false,
    usage: "[hauteur du jet] ([nombre de jets])",
	description: 'Fait un jet selon la demande.',
	execute(message, args) {
		if (args[0].charAt(1) == 'd') {
			jdrJets(message, args[0]);
		}else{
			message.delete();
			message.channel.send('Erreur jet invalide');}
	},
};

function jdrJets(message, jet) {
	let mesNb = new Array();
	mesNb = jet.split('d');
	try{
		const embed = new Discord.RichEmbed()
			.setTitle('Lancé de ' + parseInt(mesNb[0], 10) + ' dé(s) :')
  			.setColor(3447003)
  			.setThumbnail('https://media.giphy.com/media/1k0YV8F2dwoJ2vXGYe/giphy.gif');

  		if (isNaN(mesNb[0]) || isNaN(mesNb[1])) {
  			throw 'error mdr';
  		}
  		if (parseInt(mesNb[0], 10) > 25) {
  			message.channel.send('Trop de jets! J\'ai la flemme bro...');
  		}else if (parseInt(mesNb[1], 10) >= 100000) {
  			message.channel.send(mesNb[1] + '!? That\'s too much!');
  		}else if (parseInt(mesNb[0], 10) < 0 || parseInt(mesNb[1], 10) < 0) {
  			message.channel.send('Pourquoi être si négatif?');
  		}else if (parseInt(mesNb[0], 10) === 0 || parseInt(mesNb[1], 10) === 0) {
  			message.channel.send('c nul...');
  		}else{
			for(let i = 1 ; i < parseInt(mesNb[0], 10) + 1; i++) {
				embed.addField('Jet n°' + i + ' :', +(Math.floor(Math.random() * Math.floor(mesNb[1])) + 1), true);
			}
			message.channel.send({ embed });
		}
	}catch(error) {
		console.log(error);
		message.channel.send('Couillon fait une bonne syntaxe! XdX X étant un nombre');
	}
}
