const url = 'http://localhost:3000';
const pathuser = "/user/";

module.exports = {
    user: {
        getAll:   ()   => { return url + pathuser },
        get:      (id) => { return url + pathuser + id},
        delete:   (id) => { return url + pathuser + id},
        post:     () => { return url + pathuser },
        put:      (id) => { return url + pathuser +id}
    },
}