const express = require('express');
const app = express();
const port = 3000;
const bp = require('body-parser');

app.use(bp.json());

console.log('====================================\n\n\tLaunching Database ! \n\n====================================\n\n')

const fs = require('fs');

var readRoutes = (dir, app) => {
    var path = path || require('path');
    fs.readdirSync(dir).forEach( (file) => {
        if (fs.statSync(path.join(dir, file)).isDirectory()) { readRoutes(path.join(dir, file), app); } 
        else { if (file.endsWith(".js")) { require(dir + "\\" + file).forEach((r) => { app[r.method](r.url, r.func); }); } }
    })
};

readRoutes(__dirname + "\\routes",app)

app.use( (err,req,res,next) => { res.status(500).send(err.message); });

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
