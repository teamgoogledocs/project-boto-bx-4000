const db = require('../modelsLoader');

module.exports = {
    get_all: (req,res,next) =>
    {
        console.log("getting all users")
        db.User.findAll().then( (users) => {
            res.json(users);
        });
    },

    create: (req,res,next) =>{
        console.log("posting a user")

        if ( req.body.pseudo && req.body.discord_id ){
            db.User.findByPk(req.body.discord_id)
            .then((obj) => {
                if(!obj){ 
                    db.User.create({
                        pseudo: req.body.pseudo, 
                        discord_id: req.body.discord_id 
                    })
                    .then( (data) => res.status(201).json(data))
                } else {
                    res.status(201).json({"estDansBdd":true})
                }
            })
            .catch( (err) => { next(err) } );;
        }
    },

    get_by_id: (req,res,next) =>{
        console.log("getting one user")
        db.User.findByPk(req.params.user_id)
        .then( (user) => {
            if (user){
                res.status(201).json(user);
            } else {
                res.status(404).json({"error":"User not found"});
            }
        })
        .catch( (err) => { next(err) } );
    }, 

    update_by_id: (req,res,next) =>{
        console.log("getting one user")
        
        if ( req.body.pseudo ){
            db.User.findByPk(req.params.user_id)
            .then((obj) => {
                if(!obj){ res.status(404).json({"error":"User not found"});}
                else { obj.update({ pseudo : req.body.pseudo }).then( (obj) => res.status(201).json(obj)) }
            })
            .catch( (err) => { next(err) } );;
        }
    }, 

    delete_by_id: (req,res,next) =>{
        console.log("deleting one user")
        db.User.findByPk(req.params.user_id)
        .then((obj) => {
            if (obj){
                res.status(201).json(obj);
                obj.destroy();
            } else { 
                res.status(404).json({"error":"User not found"});
            }
        })
        .catch( (err) => { console.log(err); next(err) } );
    },

	load_by_id: (req, res, next) => {
        console.log("updating one user")
		return db.User.findByPk(req.params.user_id)
		.then((u) => {
			if (!u) {
				if(!u){ 
                    res.status(404).json({"error":"User not found"});
                };
			}
            req.user = u;
            next();
		})
		.catch((err) => next(err));
	}
}