const user = require('../controllers/c_User.js');

module.exports = [
    // url est l'addresse a appeler pour pouvoir faire la requête
    // :user_id est un paramètre que l'on peut transmettre qui sera reconnu par express par exemple 298155084397805578 pour traiter Olivier
    // method montre la méthode qui sera utilisé pour répondre à la requête
    { url: '/user', method: 'get', func: user.get_all },
    { url: '/user', method: 'post', func: user.create },
    { url: '/user/:user_id', method: 'get', func: user.get_by_id },
    { url: '/user/:user_id', method: 'put', func: user.update_by_id },
    { url: '/user/:user_id', method: 'delete', func: user.delete_by_id },
];

/* autre cas de figure ont veut obtenir le personnage d'id 2 de Thomas
pour cela il faudra require le controlleur personnage et la route sera sous la forme :
    { url: '/user/:user_id/pers/:pers_id', method: 'get', func: [ user.load_by_id, pers.get_by_id ] },
*/
