module.exports = function (sequelize, DataTypes) {
    const User = sequelize.define('User', {
        pseudo: DataTypes.STRING,
        discord_id: {
            type: DataTypes.STRING,
            primaryKey: true
        }
    });

    User.associate = (db) => {
    };

    User.editableFields = [ 'pseudo' ];

    return User;
};
