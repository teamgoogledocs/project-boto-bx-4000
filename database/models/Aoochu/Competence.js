module.exports = function (sequelize, DataTypes) {
    const Competence = sequelize.define('Competence', {
        nom: DataTypes.STRING,
        description: DataTypes.STRING,
        cout_PM: DataTypes.INTEGER
    });
    
    Competence.associate = (db) => {};

    return Competence;
};
