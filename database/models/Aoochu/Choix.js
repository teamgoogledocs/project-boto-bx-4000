module.exports = function (sequelize, DataTypes) {
    const Choix = sequelize.define('Choix_specialite', {
        nom: DataTypes.STRING,
        description: DataTypes.STRING,
    });
    
    Choix.associate = (db) => {};

    return Choix;
};
