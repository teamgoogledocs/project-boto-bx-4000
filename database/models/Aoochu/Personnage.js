module.exports = function (sequelize, DataTypes) {
    const Personnage = sequelize.define('Personnage', {
        nom: DataTypes.STRING,
        description: DataTypes.STRING,
        eau: DataTypes.INTEGER,
        feu: DataTypes.INTEGER,
        air: DataTypes.INTEGER,
        terre: DataTypes.INTEGER
    });

    Personnage.associate = (db) => {
        Personnage.belongsTo(db.User, { onDelete: 'CASCADE'})
        Personnage.belongsTo(db.Job, { as: 'id_job_primaire'})
        Personnage.belongsTo(db.Job, { as: 'id_job_secondaire'})
    };

    return Personnage;
};
