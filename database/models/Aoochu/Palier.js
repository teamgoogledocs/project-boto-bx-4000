module.exports = function (sequelize, DataTypes) {
    const Palier = sequelize.define('Palier', {
        nom: DataTypes.STRING,
        description: DataTypes.STRING,
        bonus_pv: DataTypes.INTEGER,
        hp_increase_interval: DataTypes.INTEGER,
        bonus_pm: DataTypes.INTEGER,
        mp_increase_interval: DataTypes.INTEGER,
        type: DataTypes.ENUM('Principale','Secondaire')
    });

    Palier.associate = (db) => {
        Palier.hasMany(db.Competence, { onDelete: 'CASCADE' });
    };

    return Palier;
};
