module.exports = function (sequelize, DataTypes) {
    const Status = sequelize.define('Status', {
        nom: DataTypes.STRING,
        description: DataTypes.STRING,
    });

    Status.associate = (db) => {};

    return Status;
};
