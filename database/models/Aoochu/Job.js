module.exports = function (sequelize, DataTypes) {
    const Job = sequelize.define('Job', {
        nom: DataTypes.STRING,
        description: DataTypes.STRING,
        bonus_pv: DataTypes.INTEGER,
        hp_increase_interval: DataTypes.INTEGER,
        bonus_pm: DataTypes.INTEGER,
        mp_increase_interval: DataTypes.INTEGER,
        type: DataTypes.ENUM('Principale','Secondaire')
    });

    Job.associate = (db) => {
        Job.hasMany(db.Palier, { onDelete: 'CASCADE' });
    };

    return Job;
};
