module.exports = function (sequelize, DataTypes) {
    const Objet = sequelize.define('Objet', {
        nom: DataTypes.STRING,
        description: DataTypes.STRING,
        prix: DataTypes.INTEGER
    });

    Objet.associate = (db) => {};

    return Objet;
};
