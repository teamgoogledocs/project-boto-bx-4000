module.exports = function (sequelize, DataTypes) {
    const Entite = sequelize.define('Entite', {
        nom: DataTypes.STRING,
        description: DataTypes.STRING,
        eau: DataTypes.INTEGER,
        feu: DataTypes.INTEGER,
        air: DataTypes.INTEGER,
        terre: DataTypes.INTEGER,
        affiliation: DataTypes.STRING
    });

    Entite.associate = (db) => {};

    return Entite;
};
