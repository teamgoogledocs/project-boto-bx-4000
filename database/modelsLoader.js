const
fs = require('fs'),
Sequelize = require('sequelize');


const { database } = require('../config.json');

// create Sequelize instance
const sequelize = new Sequelize(database.name, database.user , database.password, {
    host: 'localhost',
    dialect: 'mysql',

    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    },

    operatorsAliases: false
  });


const db = {};

var readModels = (dir, db) => {
  var path = path || require('path');
  fs.readdirSync(dir).forEach( (file) => {
    if (fs.statSync(path.join(dir, file)).isDirectory()) { readModels(path.join(dir, file), db); } 
    else { if (file.endsWith(".js")) {const model = sequelize.import(dir + "\\" + file); db[model.name] = model;} }
  })
};

readModels(__dirname + "\\models",db)

Object.keys(db).forEach((modelName) => {
	db[modelName].associate(db);
});

sequelize.sync();

module.exports = db;
